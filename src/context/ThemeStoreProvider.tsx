import React from 'react'

type ThemeContextType = {
    theme: stColor,
    setTheme: (theme: stColor) => void;
};

type stColor = {
    bgValue:color,
    variantValue:color
}
type color = 'light' | 'dark'

// let test: color = 'light' as color;

export const ThemeContext = React.createContext<ThemeContextType|undefined>(undefined)

export const ThemeStoreProvider = ({ children }: any) => {
    const [theme, setTheme] = React.useState<stColor>({bgValue:"light" ,variantValue:"light"})
    return (
        <ThemeContext.Provider value={{theme,setTheme}} >
            {children}
        </ThemeContext.Provider>
    )
}

export default ThemeStoreProvider
