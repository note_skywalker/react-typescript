

export interface AuthToken {
    appToken: string,
    tokenType:string
}

export interface ProfileInterface {
    username: string,
    mobile_no: string
}
export interface IPagination {
    page: number,
    limit: number,
    sort_by?: {
        sortField?: string,
        sortOrder?: string
    }
}

export interface IResponseData {
    status: string,
    data: {
        sample: []
    },
    pagination: {
        currentPage: number,
        nextPage?: number,
        pageCount: number,
        itemCount: number,
        pages: [],
        limit: number
    }
}

export interface ICart {
    cart: [],
    total: number
}



export interface ICartDetail  {
    title: string,
    developer: string,
    thumbnail?: string,
    qty: number
}

export interface ICartArray extends Array<ICartDetail> {}
export type TypeCartArray = ICartDetail[]

export interface IManagePageTable {
    id: number,
    title: string,
    short_description: string,
    developer: string,
    release_date: string,
    thumbnail?: string
}

export interface IRefObject {
    callApi: (url:string,input:object) => object
}

export interface ISendOtp {
    mobile_no:string
}

