import React from 'react'
import { Route,Redirect,RouteProps } from 'react-router'

import { useSelector } from 'react-redux'
import { RootState } from '../redux/reducers';

interface PrivateRouteProps extends RouteProps{
    children : any ;
}

export default function PrivateRoute({children , ...rest}: PrivateRouteProps ){
    let isLogin = false
    const checkAppToken = useSelector((state: RootState) => state.authReducer.appToken)
    if(checkAppToken){
        isLogin = true
    }
    return (
        <Route 
            {...rest}
            render={({location})=>
                isLogin ? (
                    children
                ):(
                 <Redirect to={{
                     pathname:'/login',
                     state:{from:location}
                 }} />
                )
            }
        />
    )
}
