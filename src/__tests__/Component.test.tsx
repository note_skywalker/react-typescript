// import { act, fireEvent, getByTestId, screen } from '@testing-library/react';
import { PageName } from '../components/PageName'
import ReactTypingEffect from 'react-typing-effect';
import LoginUsername from '../components/LoginUsername'
import LoginMobile from '../components/LoginMobile';


import Enzyme from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

import { shallow, mount, render } from 'enzyme';

Enzyme.configure({ adapter: new Adapter() });


test('shallow LoginUsername', () => {
  const component = shallow(<LoginUsername />)
  const form = component.find('form') // หาฟอร์ม 
  expect(form.length).toBe(1)   // ควรมี 1 form

  expect(form.find('button').length).toBe(1)  // หา <button ใน ฟอร์ม

  expect(component.find('label').length).toBe(2)
  expect(form.find('div.form-group').length).toBe(2)  // <div className="form-group"

  expect(form.find('input').length).toBe(2)

})


test('mount LoginMobile' , ()=>{
  const component = mount(<LoginMobile send="false" />)
  const form = component.find('form')
  expect(form.length).toBe(1)
  expect(form.find('input').length).toBe(1)
  const button = form.find('button')
  expect(button.props().type).toBe('submit')
  expect(button.props().type).not.toBe('button')
  // .not.toHaveAttribute('type', 'button')
  // expect(button).toHaveAttribute('type', 'submit')

  // toBeTruthy  "" {} return true
})


describe('<PageName />', () => {
  beforeEach(()=>{
    
  })

  it('shallow <PageName /> component', () => {

    const wrapper = shallow(<PageName name="test" />)
    expect(wrapper.find(ReactTypingEffect)).toHaveLength(1)
  })

})

// describe('Parent Component', () => {
//   it('renders Child component', () => {
//     const wrapper = shallow(<Parent />);
//     expect(wrapper.find(Child)).toHaveLength(1);
//   });
// });

// let wrapper1 = mount(<PersonalInfo {...personalInfodata} />);
//     expect(wrapper1.find('ChildComponent').children(0).find('.description').text().length).toBeGreaterThan(0);