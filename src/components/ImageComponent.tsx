
import PropTypes from 'prop-types';

const ImageComponent = (props: any) => {
    // { height: 225, width: '100%', display: 'block', objectFit: 'cover' }
    return (
        <>
            
            <img className={props?.addClass}
                src={props.src} 
                style={props?.style}
                {...props?.attr}
            />
        </>
    )
}

ImageComponent.propTypes = {
    addClass: PropTypes.string,
    src:PropTypes.string,
    style:PropTypes.object,

    attr:PropTypes.object
    

    
}

// type Props = {
//     alt: string,
//     src: string | boolean | Function,
//     className?: string,
//     style: React.Element<typeof StyledImage>
//   };

// const StyledImage = styled.img`
//   max-width: ${props => props.imageWidth || 100}%;
// `;

export default ImageComponent
