import React from 'react'
import { Navbar, Nav } from 'react-bootstrap'
import {
    NavLink, useHistory
} from "react-router-dom";

import { ThemeContext } from '../context/ThemeStoreProvider'

import { useSelector, useDispatch } from 'react-redux'
import { updateProfile } from '../redux/actions/profileAction';
import { updateToken } from '../redux/actions/authAction';

import { RootState } from '../redux/reducers';


const NavBar = () => {
    const themeContext = React.useContext(ThemeContext)

    const history = useHistory()

    //redux
    const profileRedux = useSelector((state: RootState) => state.profileReducer)
    const cartTotalRedux = useSelector((state: RootState) => state.cartReducer.total)
    const dispatch = useDispatch()
    const logout = () => {
        // localStorage.clear()
        history.replace('/')
        // profileData?.setProfile('')
        // history.go(0)

        dispatch(updateProfile('', ''))
        dispatch(updateToken('', ''))
    }

    // React.useEffect(() => {
    //     // getProfile()
    //     console.log("Nav UseEffect");
    // }, [])

    return (
        <>
            <Navbar bg={themeContext?.theme.bgValue} variant={(themeContext?.theme.variantValue) ? themeContext?.theme.variantValue:'light'} expand="lg">

                <NavLink className="navbar-brand" to="/" exact={true}>
                    <Navbar.Brand > Nonthawat </Navbar.Brand>
                </NavLink>

                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <NavLink className="nav-link" to="/" exact activeClassName="active">
                            Home
                        </NavLink>
                        <a className="nav-link" href={`${process.env.REACT_APP_IPBACKEND}/api-docs`} target="_blank" rel="noopener noreferrer">
                            Swagger
                        </a>

                        <NavLink className="nav-link" to="/manage" exact activeClassName="active">
                            Manage
                        </NavLink>

                        {
                            cartTotalRedux ? (
                                <>
                                    <NavLink className="nav-link" to="/total" exact activeClassName="active">
                                        Total Carts : {cartTotalRedux}
                                    </NavLink>
                                </>
                            ) : (<></>)
                        }
                    </Nav>

                    <Nav className="ml-auto">
                        {
                            profileRedux.username || profileRedux.mobile_no ? (
                                <>
                                    <span className="navbar-text text-dark">Welcome: {profileRedux.username ? profileRedux.username:profileRedux.mobile_no}</span>
                                    <button className="btn btn-outline-danger ml-2" onClick={logout}>Logout</button>
                                </>
                            ) : (
                                <>
                                    <NavLink className="nav-link" to="/login" activeClassName="active">
                                        Login
                                    </NavLink>
                                </>
                            )
                        }



                    </Nav>

                </Navbar.Collapse>
            </Navbar>
        </>
    )
}

export default NavBar
