import React from 'react'
import PropTypes from 'prop-types';
import ReactTypingEffect from 'react-typing-effect';

import { Form } from 'react-bootstrap'
import { ThemeContext } from '../context/ThemeStoreProvider'

export const PageName = (props: any) => {

    const name = React.useMemo(() => {
        return props.name
    }, [props.name])

    //Context
    const themeContext = React.useContext(ThemeContext)
    const customSwitch = false
    const changeContext = () => {
        if (themeContext?.theme.bgValue === 'light') {
            themeContext?.setTheme({ bgValue: "dark", variantValue: "dark" })
        } else {
            themeContext?.setTheme({ bgValue: "light", variantValue: "light" })
        }

    }

    return (
        <>
            <div className="container mt-3">
                {/* <h3>{props.name}</h3> */}
              
                <div className="d-flex">
                    <div className="mr-auto p-2">
                    <h3>
                    <ReactTypingEffect
                        // text={[props.name]}
                        text={[name]}
                        speed={200}
                    />
                </h3>
                    </div>
                    <div className="p-2">
                        <Form.Check
                            disabled={customSwitch}
                            onClick={changeContext}
                            type="switch"
                            id="custom-switch"
                            label="Theme"
                        />
                    </div>
                </div>

            </div>
        </>

    )
}

PageName.propTypes = {
    name: PropTypes.string.isRequired
}
