import React from 'react'
import { Spinner } from 'react-bootstrap'


const SpinnerComponent = () => {
    return (
        <>
                {/* <Spinner animation="border" variant="primary"   as="span"  /> */}
                <div className="text-center mt-10">
                    <Spinner animation="border" variant="primary" />
                    <Spinner animation="border" variant="secondary" />
                    <Spinner animation="border" variant="success" />
                    <Spinner animation="border" variant="danger" />
                    <Spinner animation="border" variant="warning" />
                    <Spinner animation="border" variant="info" />
                    <Spinner animation="border" variant="dark" />
                </div>
        </>
    )
}

export default SpinnerComponent
