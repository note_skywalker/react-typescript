import PropTypes from 'prop-types';

import DataTable from 'react-data-table-component';

const TableComponent = (props: any) => {


    const data = props.tableData
    const columns = props.headers

    const onChage = (pageNumber: number) => {
        props.handlePageChange(pageNumber)
    }

    const rowClick = (data:any) =>{
        console.log("rowClick: ",data)
    }
    
    const sort = (res:any) =>{
        console.log("sort: ",res)
        return data
    }


    return (
        <DataTable
            // title="Arnold Movies"
            columns={columns}
            data={data}
            onChangePage={onChage}

            // progressPending={loading}
            pagination
            paginationServer
            paginationTotalRows={props.total}
            paginationComponentOptions={{noRowsPerPage: true}}
            onRowClicked={rowClick}

            onSort={sort}
            sortServer={true}
            pointerOnHover
            striped
            highlightOnHover
        />
    )

    // return (
    //     <>
    //         <table className="table table-striped">
    //             <thead>
    //                 <TableHeader headers={props.headers} />
                
    //             </thead>
    //             <tbody>
    //                 <TableBody tableBody={props.tableData} />
    //             </tbody>
    //         </table>
    //     </>
    // )
}

TableComponent.propTypes = {
    tableData: PropTypes.array,
    headers: PropTypes.array,
    total:PropTypes.number,
    handlePageChange: PropTypes.func
}



const TableBody = (props: any) => {
    return (
        <tr>
            <td>{props.title}</td>
            <td>{props.short_description}</td>
        </tr>
    );
}

const TableHeader = (props: any) => {
    // const header = Object.keys(props.headers)
    console.log("TableHeader: ", props.headers);
    return (
        <tr>
            {
                props.headers.map((res: any, index: number) => {
                    return <th key={index}>{res}</th>
                })
            }
        </tr>
    )
}



export default TableComponent
