import React from 'react'

import { Modal } from 'react-bootstrap'
import ImageComponent from './ImageComponent'

import { format } from 'date-fns'
import {  enUS } from 'date-fns/locale'

export const ModalViewComponent = (props: any) => {

    // const [lgShow, setLgShow] = React.useState(false);

    // React.useEffect(()=>{
    //     setLgShow(props.show)
    //     console.log("modalprops.showData: ",props.show)
    //     console.log("modalData: ",props.modalData)
    // },[lgShow])
    const [modifyDate, setModifyDate] = React.useState('')

    React.useEffect(() => {
        if (props.modalData.release_date) {
            const d = format(new Date(props.modalData.release_date), 'dd MMMM yyyy', { locale: enUS })
            setModifyDate(d)
        }

    }, [props.modalData])


    return (
        <>
            <Modal
                size="lg"
                show={props.show}
                onHide={() => props.setModal(false)}
                aria-labelledby="example-modal-sizes-title-lg"
            >
                <Modal.Header closeButton>
                    <Modal.Title id="example-modal-sizes-title-lg">
                        {props.modalData.title}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="text-center">
                        <ImageComponent src={props.modalData.thumbnail} style={{ padding: '5px', objectFit: 'cover', width: '70%' }} attr={{ height: "300px" }} />
                    </div>
                    <hr />
                    <div className="p-3">
                        <h6>Short Description: </h6>
                        <p>{props.modalData.short_description}</p>
                        <h6>Developer: </h6>
                        <p>{props.modalData.developer}</p>
                        <h6>Release Date: </h6>

                        <p>{modifyDate}</p>
                    </div>

                </Modal.Body>
            </Modal>
        </>
    )
}
