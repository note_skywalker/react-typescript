
import { FaAngleLeft, FaAngleRight, FaAngleDoubleRight, FaAngleDoubleLeft } from 'react-icons/fa'
import Pagination from "react-js-pagination";
import PropTypes from 'prop-types';


const PaginationComponent = (props: any) => {

    const onChagePage = (pageNumber: number) => {
        props.handlePageChange(pageNumber)
    }

    return (
        <>
            <div className="d-flex">
                <div className="mr-auto p-2"></div>
                <div className="p-2">
                    <Pagination
                        activePage={props.page}
                        itemsCountPerPage={10}
                        totalItemsCount={props.total}
                        // pageRangeDisplayed={15}
                        onChange={onChagePage}
                        itemClass="page-item"
                        linkClass="page-link"
                        prevPageText={<FaAngleLeft />}
                        nextPageText={<FaAngleRight />}
                        firstPageText={<FaAngleDoubleLeft />}
                        lastPageText={<FaAngleDoubleRight />}
                    ></Pagination>
                </div>
            </div>
        </>
    )
}

PaginationComponent.propTypes = {
    page: PropTypes.number,
    total:PropTypes.number,
    handlePageChange:PropTypes.func
    
}


export default PaginationComponent
