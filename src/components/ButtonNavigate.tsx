import React from 'react'
import { useHistory } from 'react-router-dom'

type urlType = {url:string , name?:string}


export const ButtonBack = () => {
    const history = useHistory()
    const goBack = ()  =>{
        history.goBack()
    }
    return (
        <>
              <button className="btn btn-outline-secondary  ml-2" onClick={goBack}>Back</button>
        </>
    )
}

export const ButtonGo = (props:urlType) => {
    const history = useHistory()
    const go = ()  =>{
        history.push(props.url)
    }

    return (
        <>
              <button className="btn btn-outline-primary  ml-2" onClick={go}>{props.name}</button>
        </>
    )
}

export const ButtonGoReplace = (props:urlType) =>{
    const history = useHistory()
    const goReplace = ()  =>{
        history.replace(props.url)
    }

    return (
        <>
              <button className="btn btn-outline-primary  ml-2" onClick={goReplace}>{props.name}</button>
        </>
    )
}

// history.push('/form/create')

