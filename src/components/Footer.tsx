import React from 'react'
// import axios from 'axios'
import { useQuery  /*,useQueryClient*/ } from 'react-query'

const Footer = () => {

    const getStarWar = async () => {
        const controller = new AbortController()
        const signal = controller.signal
        try {
            const promise = await fetch("https://swapi.dev/api/films/", {
                method: 'get',
                signal: signal
    
            }).then(res => res.json())
            promise.cancel = () => controller.abort()
    
            // return promise as PromiseWithCancel<any>;
            if(promise.results.length > 0){
                return promise.results[2]
            }else{
                return {title:'Loading Error'}
            }
        } catch (error) {
            return {title:'Loading Error'}
        }
       
       
    }
    const { data, status, error } = useQuery("films", getStarWar)

    const memoizedResult = React.useMemo(() => {
        console.log("memo: ");
        return data
    }, [data]);


    React.useEffect(() => {
        console.log("memoizedResult: ", memoizedResult);
    }, [memoizedResult])
    // if (status === 'loading') {
    //     return <span>Loading...</span>
    // }

    // if (status === 'error') {
    //     return <span>Error: {error}</span>
    // }

    // if(status ==='success'){
    //     return (
    //         <>
    //         {
    //             data.results.map((res:any,index:number)=>{
    //                 return <span key={index}>{res.title}{' '}</span>
    //             })
    //         }
    //         </>
    //     )
    // }


    return (
        // <div>
        <footer className="container">

            <div className="d-flex">
                <div className="mr-auto p-2">
                    <p>© Nonthawat Website 2021</p>
                </div>
                <div className="p-2">
                    {
                        (status ==='success') && (
                            <>
                               React Query {memoizedResult.title}
                            </>
                        )
                    }
                        {
                        (status ==='loading') && (
                            <>
                               React Query loading ...
                            </>
                        )
                    }
                        {
                        (status ==='error') && (
                            <>
                               React Query Error : {error}
                            </>
                        )
                    }

                
                </div>
            </div>


        </footer>
        // </div>
    )
}

export default Footer
