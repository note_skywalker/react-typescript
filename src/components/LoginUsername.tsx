import React from 'react'
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";

import axios from 'axios'

import SpinnerComponent from './SpinnerComponent';
import useLoading from '../hooks/useLoading';
import ServiceMain from '../services/ServiceMain';

const schema = yup.object().shape({
    username: yup.string().required('Please Add Username'),
    password: yup.string().required('Please Add Password')
});

const LoginUsername = (props:any) => {
    const { register, handleSubmit, formState: { errors } } = useForm({
        resolver: yupResolver(schema)
    });



    const [loading, setloading] = useLoading()

    const login = async (data: object) => {

        setloading(true)
        try {
            const result = await axios.post(`${process.env.REACT_APP_BACKEND}/auth/login`, data)
            if (result.data.status === 200) {
                setloading(false)
                const value = result.data.data
                props.updateAuth(value)
    
            }
        } catch (error) {
            setloading(false)

            ServiceMain.swalAlert('Error',error.response.data.message,'error')
            
        } finally { setloading(false) }
      
    }

    return (
        <>
            <form className="col-md-8 col-sm-12" onSubmit={handleSubmit(login)} >

                <div className="form-group">
                    <label>Username</label>
                    <input  type="text"  {...register('username')} placeholder="Enter Username" className={`form-control ${errors.username ? 'is-invalid' : ''}`} />
                    {
                        errors.username && (
                            <div className="invalid-feedback">{errors.username?.message}</div>
                        )
                    }
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" {...register('password')} placeholder="Enter password" className={`form-control ${errors.password ? 'is-invalid' : ''}`} />
                    {
                        errors.password && (
                            <div className="invalid-feedback">{errors.password?.message}</div>
                        )
                    }
                </div>
                {
                    loading ? (
                        <>
                            <SpinnerComponent />
                        </>
                    ):(
                        <>
                         <button type="submit" className="btn btn-primary btn-block"> Login </button>
                        </>
                    )
                }
               
            </form>
        </>
    )
}

export default LoginUsername
