import React from 'react'
import axios from 'axios'
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";


import { IRefObject, ISendOtp } from '../interface/IMain'


import SpinnerComponent from './SpinnerComponent';
import useLoading from '../hooks/useLoading';

import ServiceMain from '../services/ServiceMain'

const LoginMobile = React.forwardRef((props: any, ref: React.Ref<IRefObject>) => {


    React.useImperativeHandle(ref, () => ({ callApi }));


    const callApi = async (url: string, input: object) => {
        try {
            const result = await axios.post(`${process.env.REACT_APP_BACKEND}/auth/${url}`, input)
            return result.data
        } catch (error) {
            ServiceMain.swalAlert('error',error.response.data.message,'error')
            return error.response.data.message
        }
    }


    const updateAuth = (value: any) => {
        props.updateAuth(value)
    }


    return (
        <>
            {

                !props.send ? (
                    <>
                        <SendOtp handleSubmit={callApi} setMobileOtp={props.setMobileOtp} setSend={props.setSend} setSendText={props.setSendText} />
                    </>) : (
                    <>
                        <VerifyOtp handleSubmit={callApi} mobileNo={props.mobileOtp} updateAuth={updateAuth} />
                    </>
                )
            }

        </>
    )
})



const SendOtp = (props: any) => {
    const regEx = /^[0]{1}[0-9]{9}$/g
    const schemaLoginMobileNo = yup.object().shape({
        mobile_no: yup.string().required('Please Add Mobile').min(10, 'Mobile number 10 digit').matches(regEx,'Format mobile only 10 digit first start only 0 Ex.0875259855')
    });

    const { register, handleSubmit, formState: { errors } } = useForm({
        resolver: yupResolver(schemaLoginMobileNo)
    });


    const [loading, setloading] = useLoading()

    const sendOtp = async (input: ISendOtp) => {
        // ^[0-9]{10}$
        setloading(true)

        try {
            const result = await props.handleSubmit('sendOtp', input)
            if (result.status === 200) {
                props.setSend(true)
                props.setMobileOtp(input.mobile_no)
                props.setSendText(result.data)

                setloading(false)
            }
        } catch (error) { 

            setloading(false)
        } finally {
            setloading(false)
         }

    }
    return (
        <>
            <form className="col-md-8 col-sm-12" onSubmit={handleSubmit(sendOtp)} >

                <div className="form-group">
                    <label>Mobile No </label>
                    <input type="text"  {...register('mobile_no')} maxLength={10} className={`form-control ${errors.mobile_no ? 'is-invalid' : ''}`} placeholder="Enter Mobile No " />

                    {
                        errors.mobile_no && (
                            <div className="invalid-feedback">{errors.mobile_no?.message}</div>
                        )
                    }
                </div>
                {
                    loading ? (
                        <>
                            <SpinnerComponent />
                        </>
                    ) : (
                        <>
                            <button type="submit" className="btn btn-primary btn-block">Get OTP</button>
                        </>
                    )
                }

            </form>
        </>
    )
}


const VerifyOtp = (props: any) => {

    const schemaOTP = yup.object().shape({
        code: yup.string().required('Please Add OTP').min(6, 'OTP have 6 digit')
    });
    const { register, handleSubmit, formState: { errors } } = useForm({
        resolver: yupResolver(schemaOTP)
    });


    const [loading, setloading] = useLoading()

    const verifyOtp = async (data: object) => {
        setloading(true)
        // mobile_no code
        const input = {
            "mobile_no": props.mobileNo,
            ...data
        }
        // console.log("input: ", input);
        try {
            const result = await props.handleSubmit('verifyOtp', input)
            if (result.status === 200) {
                setloading(false)
                props.updateAuth(result.data)
            }
        } catch (error) {
            setloading(false)

        } finally { setloading(false) }

    }



    return (
        <>
            <form className="col-md-8 col-sm-12" onSubmit={handleSubmit(verifyOtp)} >

                <div className="form-group">
                    <label> </label>
                    <input type="text"  {...register('code')} placeholder="Enter OTP" maxLength={6} className={`form-control ${errors.code ? 'is-invalid' : ''}`} />
                    {
                        errors.code && (
                            <div className="invalid-feedback">{errors.code?.message}</div>
                        )
                    }
                </div>


                {
                    loading ? (
                        <>
                            <SpinnerComponent />
                        </>
                    ) : (
                        <>
                            <button type="submit" className="btn btn-primary btn-block">Submit OTP</button>
                        </>
                    )
                }

            </form>
        </>
    )
}


export default LoginMobile
