import React from 'react'

import { Form, Button } from 'react-bootstrap'
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";

import ImageComponent from './ImageComponent';

import SpinnerComponent from './SpinnerComponent';
import useLoading from '../hooks/useLoading';

import { format } from 'date-fns'
import { th } from 'date-fns/locale'




const FormInputComponent = (props: any) => {
    const [loading, setloading] = useLoading()

    const TYPE_PHOTO = ['image/jpg', 'image/jpeg']
    const schema = yup.object().shape({
        title: yup.string().required('Please add title'),
        short_description: yup.string().required('Please add description').max(200, 'Max 200 Char'),
        release_date: yup.date().required('Please add release date'), // date
        developer: yup.string().required('Please add developer name'),
        // price: yup.number().positive().integer().required(),

        thumbnail: yup.mixed()
            .test('name', 'File is required', value => {
                if (props.edit) {
                    return true
                } else {
                    return value[0] && value[0].name !== '';
                }

            })
            // // .test("fileSize", "File is too large", value => {
            // //     console.log("size ", value[0].size)
            // //     console.log("type ", value[0].type)
            // //     return value[0] && value[0].size <= 2000000
            // // })
            .test('type', 'We only support image', value => {
                if (props.edit) {
                    return true
                } else {
                    return value[0] && TYPE_PHOTO.includes(value[0].type)
                }
            })

    })

    const { register, handleSubmit, formState: { errors }, setValue } = useForm({
        resolver: yupResolver(schema)
    })

    const [selectedFile, setSelectedFile] = React.useState('')
    const [preview, setPreview] = React.useState('')

    const [img, setImg] = React.useState('')
    const onSubmit = async (data: any) => {
        try {
            setloading(true)
            await props.onSubmit(data, selectedFile)
            setloading(false)
        } catch (error) {
            setloading(false)
        } finally { setloading(false) }

    }

    const fileChangedHandler = (event: any) => {
        let file = event.target.files[0];
        setSelectedFile(file)
    }

    React.useEffect(() => {
        if (!selectedFile) {
            setPreview('')
            return
        }
        const objectUrl = URL.createObjectURL(selectedFile)
        setPreview(objectUrl)
        // free memory when ever this component is unmounted
        return () => URL.revokeObjectURL(objectUrl)
    }, [selectedFile])

    const getData = async () => {
        const data = await props.getSample()
        setValue('title', data.title)
        setValue('short_description', data.short_description)
        const newDate = format(new Date(data.release_date), 'yyyy-MM-dd', { locale: th })
        setValue('release_date', `${newDate}`)
        // console.log('release_date', newDate)
        setValue('developer', data.developer)
        setImg(data.thumbnail)
    }

    const {edit} = props

    React.useEffect(() => {
        // console.log("EDIT: ",props.edit)
        if (props.edit === 1) {
            getData()

        }
    }, [edit])

    return (
        <>
            <Form className="mt-3" onSubmit={handleSubmit(onSubmit)}>
                <Form.Group className="mb-3">
                    <Form.Label>Title</Form.Label>
                    <Form.Control type="text" placeholder="Title" {...register("title")} className={`form-control ${errors.title ? 'is-invalid' : ''}`} />
                    {
                        errors.title && (
                            <Form.Control.Feedback type="invalid">
                                {errors.title?.message}
                            </Form.Control.Feedback>
                        )
                    }
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Short Description</Form.Label>
                    <Form.Control type="text" placeholder="Short Description" {...register("short_description")} className={`form-control ${errors.title ? 'is-invalid' : ''}`} />
                    {
                        errors.short_description && (
                            <Form.Control.Feedback type="invalid">
                                {errors.short_description?.message}
                            </Form.Control.Feedback>
                        )
                    }
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Release Date</Form.Label>
                    <Form.Control type="date" placeholder="Release Date" {...register("release_date")} className={`form-control ${errors.title ? 'is-invalid' : ''}`} />
                    {/* min="2018-01-01" max="2018-12-31" */}
                    {
                        errors.release_date && (
                            <Form.Control.Feedback type="invalid">
                                {errors.release_date?.message}
                            </Form.Control.Feedback>
                        )
                    }
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Developer</Form.Label>
                    <Form.Control type="text" placeholder="Developer" {...register("developer")} className={`form-control ${errors.title ? 'is-invalid' : ''}`} />
                    {
                        errors.developer && (
                            <Form.Control.Feedback type="invalid">
                                {errors.developer?.message}
                            </Form.Control.Feedback>
                        )
                    }
                </Form.Group>

                <div className="form-group">
                    <label htmlFor="exampleFormControlFile1">Thumbnail</label>
                    <input type="file"
                        className={`form-control-file ${errors.thumbnail ? 'is-invalid' : ""}`}
                        {...register("thumbnail")}
                        onChange={fileChangedHandler}
                        name="thumbnail"
                        id="exampleFormControlFile1" />
                    {
                        errors.thumbnail && <p className="invalid-feedback">{errors.thumbnail?.message}</p>
                    }
                </div>

                {
                    preview && (
                        <>
                            <div className="form-group">
                                <ImageComponent src={preview} attr={{ "width": 100, "height": 150 }} />

                            </div>
                        </>
                    ) 
                }

                {
                    (img && !preview) ? (
                        <>
                        <div className="form-group">
                            <ImageComponent src={img} attr={{ "width": 100, "height": 150 }} />

                        </div>
                        </>
                    ): (
                        <></>
                    )
                }

                {
                    loading ? (
                        <>
                            <SpinnerComponent />
                        </>
                    ) : (
                        <>
                            <Button variant="primary" className="btn-block" type="submit">
                                Submit
                            </Button>
                        </>
                    )
                }


            </Form>
        </>
    )
}

export default FormInputComponent
