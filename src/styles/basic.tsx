import styled from 'styled-components'
interface PropsInterface {
    primary?: boolean
}
const TestCss = styled.div`
background: ${(props: PropsInterface) => props.primary ? "palevioletred" : "white"};
color: ${(props: PropsInterface) => props.primary ? "white" : "palevioletred"};

font-size: 1em;
margin: 1em;
padding: 0.25em 1em;
border: 2px solid palevioletred;
border-radius: 3px;
`

export { TestCss }