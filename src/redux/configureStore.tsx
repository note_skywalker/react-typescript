import { createStore, applyMiddleware } from 'redux';

import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
import rootReducer from './reducers/index'


//thunk
import { composeWithDevTools } from 'redux-devtools-extension';  // กรณีใช้ redux persists
import thunk from 'redux-thunk';


const persistConfig = {
    key: 'reactAppPersist',
    storage,
    whitelist: ['authReducer', 'profileReducer', 'cartReducer'], // ถ้าไม่ใส่ จะเก็บทุก reducers ใน index หรือใช้ blacklist 
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

const configureStore = () => {
    // let store_persist = createStore(persistedReducer)   // ไม่มี thunk

    let store_persist :any = createStore(persistedReducer, composeWithDevTools(
        applyMiddleware(thunk)   // 1 อัน ถ้ามีหลายอัน spred opretor ได้
    ))


    let persistor = persistStore(store_persist)
    return { store_persist, persistor }
}

export default configureStore