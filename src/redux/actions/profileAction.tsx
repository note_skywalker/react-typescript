// import { Dispatch } from 'redux'

export const GET_PROFILE = 'GET_PROFILE'

export const updateProfile = (username:string,mobile_no:string) => {
    return {
        type:GET_PROFILE,
        payload:{
            username:username,
            mobile_no:mobile_no
        }
    }
}
    