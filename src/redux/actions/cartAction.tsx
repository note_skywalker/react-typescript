
import { ICartDetail , ICartArray} from '../../interface/IMain'

export const ADD_CART = 'ADD_CART'
export const CLEAR_ALL = 'CLEAR_ALL'

export const addToCart = (productData: ICartDetail, cart = [] as ICartArray) => {
    let findData = false;
    if (cart.length > 0) {
        for (const c of cart) {
            if (c.title === productData.title) {
                console.log("c.qty: ", c.qty);
                c.qty++
                findData = true
            }
        }
    }
    if (!findData) {
        cart.push(productData)
    }
    const total : number = cart.reduce((qty: number, product: ICartDetail) => {
        return qty += product.qty
    }, 0)


    return {
        type: ADD_CART,
        payload: {
            cart: cart,
            total: total
        }
    }
}


export const clearCart = () => {
    const cart: any = []
    const total = 0
    return {
        type: CLEAR_ALL,
        payload: {
            cart: cart,
            total: total
        }
    }
}

export const cartAction = () => {
    return
}
