import axios from 'axios'
import { Dispatch } from 'redux'

// dispatch: ({type}:{type:string}) => void;
export const GET_TOKEN = 'GET_TOKEN'

export const updateToken = (token: string,tokenType:string) => {
    return {
        type: GET_TOKEN,
        payload: {
            appToken: token,
            tokenType:tokenType
        }
    }
}

export const GET_THUNK_TEST = 'GET_THUNK_TEST'
export const thunkTest = () => {
    return async (dispatch: Dispatch) => {
        try {
            const result = await axios.get(`${process.env.REACT_APP_BACKEND2}/api/auth/getTest`)
            dispatch({
                type: GET_THUNK_TEST,
                payload: {
                    testvalue: result.data.data
                }
            })
        } catch (error) {
            dispatch({
                type: GET_THUNK_TEST,
                payload: {
                    testvalue: error
                }
            })
        }

    }
}