import { GET_TOKEN, GET_THUNK_TEST } from '../actions/authAction'

import { AuthToken } from '../../interface/IMain'

type action = { type: 'GET_TOKEN', payload: AuthToken }  
            | { type: 'GET_THUNK_TEST', payload: any }


const initState:AuthToken = {
    appToken: '',
    tokenType:''
}

const authReducer = (state = initState, action: action) => {
    switch (action.type) {
        case GET_TOKEN:
            return {
                ...state,
                appToken: action.payload.appToken,
                tokenType:action.payload.tokenType
            }
        case GET_THUNK_TEST:
            return {
                ...state,
                testvalue:action.payload.testvalue
            }
        default:
            return state
    }
}

export default authReducer