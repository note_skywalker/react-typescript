import { GET_PROFILE } from '../actions/profileAction'
import { ProfileInterface } from '../../interface/IMain'

type action = {type:'GET_PROFILE',payload:ProfileInterface}

const initState: ProfileInterface = {
    username: '',
    mobile_no: ''
}

const profileReducer = (state = initState, action: action) => {
    switch (action.type) {
        case GET_PROFILE:
            return {
                ...state,
                username : action.payload.username,
                mobile_no : action.payload.mobile_no
            }
        default:
            return state
    }
}

export default profileReducer