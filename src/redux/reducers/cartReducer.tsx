
import { ADD_CART, CLEAR_ALL } from '../actions/cartAction'
import { ICartArray } from '../../interface/IMain'

type valueCart = { cart: ICartArray, total: number }
type action = { type: 'ADD_CART', payload: valueCart }  | { type: 'CLEAR_ALL', payload: valueCart }

const initState: valueCart = {
    cart: [],
    total: 0
}

const cartReducer = (state = initState, action: action) => {

    switch (action.type) {
        case ADD_CART:
            return {
                ...state,
                cart: action.payload.cart,
                total: action.payload.total
            }
        case CLEAR_ALL:
            return {
                ...state,
                cart: action.payload.cart,
                total: action.payload.total
            }
        default:
            return state
    }
}

export default cartReducer
