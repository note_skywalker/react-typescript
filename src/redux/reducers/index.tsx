import { combineReducers } from "redux";
import authReducer from "./authReducer";
import profileReducer from "./profileReducer";
import cartReducer from "./cartReducer";

const rootReducer = combineReducers({
    authReducer,
    profileReducer,
    cartReducer,
})

export type RootState = ReturnType<typeof rootReducer>
export default rootReducer