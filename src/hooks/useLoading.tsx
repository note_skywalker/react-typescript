import React from 'react'

const useLoading = () => {
    const [loading,setloading] = React.useState(false)

    return [loading,setloading ] as const
}

export default useLoading
