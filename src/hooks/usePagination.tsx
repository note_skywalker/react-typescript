import React from 'react'
import { IManagePageTable} from '../interface/IMain'

 const usePagination = () => {

    const [page,setPage]=React.useState(1)
    const [total, setTotal] = React.useState(0)
    const [dataSample, setDataSample] = React.useState<Array<IManagePageTable>>([])
    const [limit,setLimit] = React.useState(9)

    return [
        page,setPage, 
        total,setTotal,
        dataSample,setDataSample,
        limit,setLimit,
    ] as const

}

export default usePagination
