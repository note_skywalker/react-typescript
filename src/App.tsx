import React from 'react';
// import logo from './logo.svg';
import './App.css';

import NavBar from './components/NavBar';
import Footer from './components/Footer';

import { SamplePage } from './pages/SamplePage';
import { LoginPage } from './pages/LoginPage';
import { TotalCartPage } from './pages/TotalCartPage';

import { CreatePage } from './pages/forms/CreatePage';
import { EditPage } from './pages/forms/EditPage';
import { ManagePage } from './pages/ManagePage';

import PrivateRoute from './guard/auth';

import { QueryClient, QueryClientProvider } from 'react-query'

import { ThemeStoreProvider } from './context/ThemeStoreProvider'

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import { Provider } from 'react-redux'

// ไม่ใช่ persist
// import { createStore, applyMiddleware } from 'redux'  //applyMiddleware use thunk
// import thunk from 'redux-thunk';
// import rootReducer from './redux/reducers/index'
// import rootReducer from './redux/reducers';

// const store = createStore(rootReducer)  // ไม่ใช้ persist
// const store = createStore(rootReducer, applyMiddleware(thunk)); // ไม่ใช่ persist


// persist
// import { PersistGate } from 'redux-persist/integration/react'
import configureStore from './redux/configureStore';
const { store_persist } = configureStore()

// React Query 
const queryClient = new QueryClient()

function App() {
  return (
    <Provider store={store_persist}>
      <ThemeStoreProvider>
        <QueryClientProvider client={queryClient}>
          <Router>
            <NavBar />
            <Switch>
              <Route exact path="/">
                <SamplePage />
              </Route>

              <Route path="/login">
                <LoginPage></LoginPage>
              </Route>

              <Route path="/form"
                render={({ match: { url } }) => (
                  <>
                    <Route path={`${url}/`} exact>
                      <CreatePage></CreatePage>
                    </Route>
                    <Route path={`${url}/edit/:id`}>
                      <EditPage />
                    </Route>
                  </>
                )}>
              </Route>

              <Route path="/total">
                <TotalCartPage></TotalCartPage>
              </Route>

              {/* <Route path="/register">
          <RegisterPage></RegisterPage>
        </Route> */}

              <PrivateRoute path="/manage">
                <ManagePage></ManagePage>
              </PrivateRoute>

            </Switch>
            <Footer />
          </Router>
        </QueryClientProvider>
      </ThemeStoreProvider>
    </Provider>

    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.tsx</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a>
    //   </header>
    // </div>
  );
}

export default App;
