import axios from 'axios'
import { IPagination, IResponseData } from '../interface/IMain';
import swal from 'sweetalert'

export default class ServiceMain {

    static async getSampleService(pagination:IPagination) {
        const sortField = pagination.sort_by?.sortField ? pagination.sort_by?.sortField : 'created_at';
        const sortOrder = pagination.sort_by?.sortOrder ? pagination.sort_by?.sortOrder : 'desc';
        const reqData = await axios.get(`${process.env.REACT_APP_BACKEND}/sample?page=${pagination.page}&limit=${pagination.limit}&sort_by=${sortField}:${sortOrder}`)
        const result: IResponseData = reqData.data
        return result
    }

    static async swalAlert(headText:string,subText:string,type:string){
            swal(headText, subText, type);
    }

    static async swalAlertCondition () {
      const result = await  swal({
            title: "Are you sure?",
            text: "Delete ?",
            icon: "warning",
            buttons: ["No", "Yes"],
            dangerMode: true,
          })
        //   .then((willDelete) => {
        //     if (willDelete) {
        //       return true
        //     } else {
        //       return false
        //     }
        //   });
        
          return result
    }


}