import React from 'react'
import { PageName } from '../components/PageName'
import ImageComponent from '../components/ImageComponent';

import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../redux/reducers';

import { clearCart } from '../redux/actions/cartAction';

import { ICartArray, ICartDetail } from '../interface/IMain'
import { ButtonBack } from '../components/ButtonNavigate';

export const TotalCartPage = () => {

    //redux
    const cartTotalRedux = useSelector((state: RootState) => state.cartReducer.total)
    const cartRedux = useSelector((state: RootState) => state.cartReducer.cart)
    const dispatch = useDispatch()

    const clear = ()=>{
        dispatch(clearCart())
    }
    return (
        <>
            <PageName name="Carts" />
            <div className="container mt-5">
                <div className="d-flex">
                    <div className="mr-auto p-2">
                        <ButtonBack />
                    </div>
                    <div className="p-2">
                        Total : {cartTotalRedux}
                        <button className="btn btn-outline-danger ml-2" onClick={clear}>Clear</button>
                    </div>
                </div>
                <CartTable cartData={cartRedux} />
            </div>


        </>
    )
}


// Thinking React  - - แหม 

const CartTable = (props: any) => {
    const rows: any[] = [];
    const cartData: ICartArray = props.cartData
    cartData.forEach((res, index) => {
        rows.push(
            <ProductRow product={res} key={index} />
        )
    })
    return (
        <table className="table table-striped">
            <thead className="thead-dark">
                <tr>
                    <th>Thumbnail</th>
                    <th>Title</th>
                    <th>Developer</th>
                    <th>Qty</th>
                </tr>
            </thead>
            <tbody>{rows}</tbody>
            {
                rows.length === 0 ? (<> <h3>No Data</h3> </>):(<></>)
            }
        </table>
    )
}

const ProductRow = (props: any) => {
    const product: ICartDetail = props.product
    return (
        <tr>
            <td><ImageComponent src={product.thumbnail} attr={{ width: '90px', height: '60px' }} /></td>
            <td>{product.title}</td>
            <td>{product.developer}</td>
            <td>{product.qty}</td>
        </tr>
    )
}


