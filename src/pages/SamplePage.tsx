import React from 'react'
import { PageName } from '../components/PageName'
import { IPagination, ICartDetail } from '../interface/IMain';

import { FaShoppingCart } from 'react-icons/fa'

import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../redux/reducers';
import { addToCart } from '../redux/actions/cartAction';

import ServiceMain from '../services/ServiceMain';

import PaginationComponent from '../components/PaginationComponent';
import ImageComponent from '../components/ImageComponent';
import SpinnerComponent from '../components/SpinnerComponent';

import usePagination from '../hooks/usePagination';
import useLoading from '../hooks/useLoading';
import { ModalViewComponent } from '../components/ModalViewComponent';


export const SamplePage = () => {

    // const [page, setPage] = React.useState(1)
    // const [total, setTotal] = React.useState(0)
    // const [dataSample, setDataSample] = React.useState([])
    const [loading, setloading] = useLoading()
    const [page, setPage, total, setTotal, dataSample, setDataSample] = usePagination()

    const [error, setError] = React.useState(false)

    const [modal,setModal] = React.useState(false)
    const [modalData,setModalData] = React.useState({})

    const cartRedux = useSelector((state: RootState) => state.cartReducer.cart)

    const dispatch = useDispatch()

    const getData = async (pagination: IPagination) => {
        try {
            setloading(true)
            const result = await ServiceMain.getSampleService(pagination)
            setDataSample(result.data.sample)
            setTotal(result.pagination.itemCount)
        } catch (error) {
            setloading(false)
            setError(true)
        } finally {
            setloading(false)
        }

    }

    React.useEffect(() => {
        let pagination: IPagination = {
            "page": page,
            "limit": 9,
            "sort_by": { "sortField": "id", "sortOrder": "asc" }
        }
        getData(pagination)
    }, [page])


    const add = (product: ICartDetail) => {
        const value: ICartDetail = {
            title: product.title,
            developer: product.developer,
            thumbnail: product.thumbnail,
            qty: 1
        }
        dispatch(addToCart(value, cartRedux))
    }



    const handlePageChange = (pageNumber: number) => {
        setPage(pageNumber)
    }

    const showModal = (value:boolean,data:object) =>{
        // modal,setModal
        setModal(value)
        setModalData(data)

        console.log("showModal: ",value)
    }

    if (error) {
        return (
            <>
                <PageName name="Welcome :)"></PageName>

                <div className="jumbotron">
                    <div className="container">
                        <div className="text-center mt-10">
                            <h5 className="text-danger">Something was wrong</h5>
                        </div>
                    </div>
                </div>

            </>
        )
    }
    return (
        <>
            <PageName name="Welcome :)"></PageName>

            <div className="jumbotron">
                <div className="container">
                    {
                        loading ? (
                            <>
                                <SpinnerComponent />
                            </>
                        ) : (
                            <>
                                <PaginationComponent page={page} total={total} handlePageChange={handlePageChange} />

                                <div className="row">
                                    {
                                        dataSample.map((res: any, index) => {
                                            return (
                                                <>
                                                    <div key={res.id} className="col-md-4 col-sm-12 mb-2" >
                                                        <div className="card box-shadow h-100" >

                                                            <ImageComponent addClass="card-img-top"
                                                                src={res.thumbnail ? res.thumbnail : 'https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png'}
                                                                style={{ height: 225, width: '100%', display: 'block', objectFit: 'cover' }} />
                                                            <CardDetailComponent res={res} add={add} showModal={showModal} />

                                                        </div>
                                                    </div>
                                                </>
                                            )
                                        })
                                    }
                                </div>

                                <PaginationComponent page={page} total={total} handlePageChange={handlePageChange} />
                            </>
                        )
                    }
                </div>
            </div>

            <ModalViewComponent  show={modal} modalData={modalData} setModal={setModal}  />



        </>
    )
}

const CardDetailComponent = (props: any) => {

    return (
        <>
            <div className="card-body">
                <h5> {props.res.title} </h5>
                <p className="card-text">
                    {props.res.short_description}
                </p>

            </div>
            <div className="card-footer">
                <div className="d-flex justify-content-between align-items-center">
                    <small className="text-muted">{props.res.developer}</small>
                    <div className="btn-group">
                        <button type="button" className="btn btn-sm btn-outline-primary" onClick={() => props.showModal(true,props.res)} >  View</button>

                        <button type="button" className="btn btn-sm btn-outline-success"
                            onClick={() => props.add(props.res)}
                        >Add To   <FaShoppingCart size="18" /> </button>

                    </div>

                </div>
            </div>
        </>
    )
}
