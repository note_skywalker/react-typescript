import React from 'react'

import { useHistory, useParams } from 'react-router-dom'
import axios from 'axios'
import { useSelector } from 'react-redux'
import { RootState } from '../../redux/reducers';
import ServiceMain from '../../services/ServiceMain';
import FormInputComponent from '../../components/FormInputComponent';
import { PageName } from '../../components/PageName';
import { ButtonBack } from '../../components/ButtonNavigate';

export const EditPage = () => {
    const history = useHistory()

    const { id } :any = useParams()

    const getSample = React.useCallback(async () => {
        const result = await axios.get(`${process.env.REACT_APP_BACKEND}/sample/${id}`)
        return result.data.data
    }, [id])

    // React.useEffect(()=>{
    //     // getSample()
    // },[id])
    const authRedux = useSelector((state: RootState) => state.authReducer)

    const onSubmit = async (data: any, selectedFile: any) => {
      try {
          console.log("DATA: ",data);
          console.log("FILE: ",selectedFile)
     
        delete data.thumbnail;
        const result = await axios.patch(`${process.env.REACT_APP_BACKEND}/sample/updateSample/${id}`, data,
            {
                headers: {
                    Authorization: `${authRedux.tokenType} ${authRedux.appToken}`,
                }
            })

        if (result.data.status === 200 && selectedFile) {
            let formData = new FormData()
            formData.append('thumbnail', selectedFile)
            const update = await axios.patch(`${process.env.REACT_APP_BACKEND}/sample/updateSample/${id}/uploadThumbnail`, formData, {
                headers: {
                    Authorization: `${authRedux.tokenType} ${authRedux.appToken}`,
                    'Content-Type': 'multipart/form-data'
                }
            })

            if (update.data.status === 200) {
                ServiceMain.swalAlert('Success', 'Updated', 'success')
                history.replace('/manage')
            }
        }else{
            ServiceMain.swalAlert('Success', 'Updated', 'success')
            history.replace('/manage')
        }
      } catch (error) {
        console.log("ERROR ",error.response.data.message);
        ServiceMain.swalAlert('Error', error.response.data.message, 'error')
      }
    

    }

    return (
        <>
            <PageName name="Edit" />
            <div className="container">
                <div className="d-flex">
                    <div className="mr-auto p-2">
                        <ButtonBack />
                    </div>
                    <div className="p-2">
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-8 col-xs-12">
                        <FormInputComponent edit={1}  getSample={getSample} onSubmit={onSubmit} />
                    </div>
                </div>
            </div>

        </>
    )
}
