import React from 'react'
import {  Container, Row, Col } from 'react-bootstrap'
import { useHistory } from 'react-router-dom'
import axios from 'axios'

import { PageName } from '../../components/PageName';

import { useSelector } from 'react-redux'
import { RootState } from '../../redux/reducers';
import ServiceMain from '../../services/ServiceMain';
import FormInputComponent from '../../components/FormInputComponent';

import { ButtonBack } from '../../components/ButtonNavigate';


export const CreatePage = () => {

    const history = useHistory()

    const authRedux = useSelector((state: RootState) => state.authReducer)

    const onSubmit = async (data: any, selectedFile: any) => {
        try {
            delete data.thumbnail;
            const result = await axios.post(`${process.env.REACT_APP_BACKEND}/sample/createSample`, data,
                {
                    headers: {
                        Authorization: `${authRedux.tokenType} ${authRedux.appToken}`,
                    }
                })

            if (result.data.status === 200) {

                let formData = new FormData()
                formData.append('thumbnail', selectedFile)

                const update = await axios.patch(`${process.env.REACT_APP_BACKEND}/sample/updateSample/${result.data.data.id}/uploadThumbnail`, formData, {
                    headers: {
                        Authorization: `${authRedux.tokenType} ${authRedux.appToken}`,
                        'Content-Type': 'multipart/form-data'
                    }
                })

                if (update.data.status === 200) {
                    ServiceMain.swalAlert('Success', 'Created', 'success')
                    history.replace('/manage')
                }

            }
        } catch (error) {
            console.log("ERROR ",error.response.data.message);
            ServiceMain.swalAlert('Error', error.response.data.message, 'error')
        }
    }

    return (
        <>
            <PageName name="Create"></PageName>

            <Container className="mt-4">
                <div className="d-flex">
                    <div className="mr-auto p-2">
                        <ButtonBack />
                    </div>
                    <div className="p-2">
                    </div>
                </div>
                <Row>
                    <Col xs={12} md={8} >
                        <FormInputComponent onSubmit={onSubmit} />

                    </Col>
                </Row>
            </Container>
        </>
    )
}
