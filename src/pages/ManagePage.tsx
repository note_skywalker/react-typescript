import React from 'react'
import { IPagination, IManagePageTable } from '../interface/IMain';
import { PageName } from '../components/PageName'
import TableComponent from '../components/TableComponent';
import ImageComponent from '../components/ImageComponent';
import { ButtonBack, ButtonGo } from '../components/ButtonNavigate';

import ServiceMain from '../services/ServiceMain'
import { format } from 'date-fns'
import { th } from 'date-fns/locale'

import usePagination from '../hooks/usePagination';
import { FaPen, FaTrash } from 'react-icons/fa'
import { useHistory } from 'react-router-dom'

import axios from 'axios'

export const ManagePage = () => {


    const [page, setPage, total, setTotal, dataSample, setDataSample] = usePagination()

    // const [page, setPage] = React.useState(1)
    // const [total, setTotal] = React.useState(0)
    // const [dataSample, setDataSample] = React.useState([])
    const [del,setDel] = React.useState(0)

    const getData = async (pagination: IPagination) => {
        const result = await ServiceMain.getSampleService(pagination)

        const filter: any = result.data.sample.map((res: IManagePageTable) => {
            return {
                id: res.id,
                title: res.title,
                short_description: res.short_description,
                developer: res.developer,
                release_date: res.release_date,
                thumbnail: res.thumbnail
            }
        })

        setDataSample(filter)
        setTotal(result.pagination.itemCount)
    }

    const handlePageChange = (pageNumber: number) => {
        setPage(pageNumber)
    }

    React.useEffect(() => {
        let pagination: IPagination = {
            "page": page,
            "limit": 10,
            "sort_by": { "sortField": "id", "sortOrder": "desc" }
        }
        getData(pagination)
    }, [page,del])

    const history = useHistory()

    const clickEdit = (res: any) => {
        history.push('form/edit/' + res.id)
    }



    const clickDel = async (res: any): Promise<any> => {
        const del = await ServiceMain.swalAlertCondition()
        if (del) {
            const result = await axios.delete(`${process.env.REACT_APP_BACKEND}/sample/${res.id}`)
            if (result.data.status === 200) {
                ServiceMain.swalAlert('Success', 'Deleted', 'success')
                // history.go(0)
                setDel(del=>del+1)
            }
        }

    }


    const headers = [
        // {
        //     name: 'Id',
        //     selector: (row: IManagePageTable) => row.id,
        //     sortable: true
        // },
        {
            name: 'Action',
            button: true,
            cell: (row: IManagePageTable) => {
                return (
                    <>
                        <button className="btn btn-outline-primary btn-sm mr-1" type="button" onClick={() => clickEdit(row)} ><FaPen /></button>
                        <button className="btn btn-outline-danger btn-sm" type="button" onClick={() => clickDel(row)} ><FaTrash /></button>
                    </>
                )
            }

        },
        {
            name: 'Title',
            selector: (row: IManagePageTable) => row.title,
            sortable: false
        },
        // {
        //     name: 'Short Description',
        //     selector: (row: IManagePageTable) => row.short_description,
        //     sortable: false
        // },
        {
            name: 'Developer',
            selector: (row: IManagePageTable) => row.developer,
            sortable: false
        },
        {
            name: 'Release Date',
            selector: (row: IManagePageTable) => row.release_date,
            sortable: false,
            cell: (row: any) => {
                const d = format(new Date(row.release_date), 'dd MMM yyyy', { locale: th })
                return <span>{d}</span>
            }
        },
        {
            name: 'Thumbnail',
            selector: (row: IManagePageTable) => row.thumbnail,
            sortable: false,
            cell: (row: any) => {
                return <ImageComponent style={{ padding: '5px' }} attr={{ height: "70px", width: "70px" }} src={row.thumbnail} />
            }
            // <img style={{ padding: '5px' }} height="70px" width="70px" src={row['thumbnail']} />
        },

        // 'Id', 'Title', 'Short Description', 'Developer', 'Release Date', 'Thumbnail'
    ]


    return (
        <div>
            <PageName name="ManagePage" />
            <div className="container" >
                <div className="d-flex">
                    <div className="mr-auto p-2">
                        <ButtonBack />
                    </div>
                    <div className="p-2">
                        <ButtonGo url="/form" name="Create" />
                    </div>
                </div>
                <TableComponent tableData={dataSample} headers={headers} total={total} handlePageChange={handlePageChange} />
            </div>



        </div>
    )
}


