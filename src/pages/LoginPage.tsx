import React from 'react'

import { useHistory } from 'react-router-dom';

import LoginUsername from '../components/LoginUsername';
import LoginMobile from '../components/LoginMobile';

import { useSelector } from 'react-redux'
import { RootState } from '../redux/reducers';

import { IRefObject } from '../interface/IMain'

import { useDispatch } from 'react-redux'
import { updateProfile } from '../redux/actions/profileAction';
import { updateToken } from '../redux/actions/authAction';

import { PageName } from '../components/PageName'

export const LoginPage = () => {

    const [loginType, setLoginType] = React.useState(2)  // 1 username 2 mobile
    const [loginName, setLoginName] = React.useState('Use Username') //Use Mobile

    const checkAppToken = useSelector((state: RootState) => state.authReducer.appToken)
    const history = useHistory()
    React.useEffect(() => {
        if (checkAppToken) history.replace('/manage')

    }, [checkAppToken])

    const changeLoginType = () => {

        if (loginType === 1) {
            setLoginType(2)
        } else {
            setLoginType(1)
        }
    }

    React.useEffect(() => {
        if (loginType === 1) {
            setLoginName('Use Mobile')
        } else {
            setLoginName('Use Username')
        }
    }, [loginType])


    const [send, setSend] = React.useState(false)
    const [mobileOtp, setMobileOtp] = React.useState('')
    const [sendText, setSendText] = React.useState('')

    const [disabled, setDisabled] = React.useState(true)


    const childRef = React.useRef<IRefObject>(null)

    const onButtonClick = () => {
        if (childRef.current) {
            childRef.current.callApi('sendOtp', { "mobile_no": mobileOtp })
            setDisabled(true)
        }
    }

    const dispatch = useDispatch()

    const updateAuth = (value: any) => {
        dispatch(updateToken(value.appToken, value.tokenType))
        dispatch(updateProfile(value.userInfo.username, value.userInfo.mobile_no))
        history.replace('/manage')
    }



    return (
        <>
            <PageName name="Login" />

            <div className="container">

                <div className="col-md-8 col-sm-12">
                    <div className="d-flex">
                        {
                            !send ? (
                                <>
                                    <div className="mr-auto p-2"></div>
                                    <div className="p-2">
                                        <button type="button" className="btn btn-outline-success btn-block" onClick={changeLoginType} >{loginName}</button>
                                    </div>
                                </>
                            ) : (
                                <>
                                    <div className="mr-auto p-2 align-self-center">
                                        <div className="text-secondary" > {sendText} </div>
                                    </div>
                                    <div className="p-2">
                                        <button type="button" className="btn btn-outline-success btn-block" onClick={onButtonClick} disabled={disabled} >
                                            Send OTP Again
                                            {
                                                disabled ? (
                                                    <>
                                                        <Timer timeCountDown={60} setDisabled={setDisabled} />
                                                    </>
                                                ) : (
                                                    <></>
                                                )
                                            }


                                        </button>
                                    </div>
                                </>
                            )
                        }

                    </div>
                </div>


                {
                    loginType === 1 ? (
                        <>
                            <LoginUsername updateAuth={updateAuth}></LoginUsername>
                        </>
                    ) : (
                        <>
                            <LoginMobile
                                ref={childRef}
                                send={send}
                                setSend={setSend}
                                mobileOtp={mobileOtp}
                                setMobileOtp={setMobileOtp}
                                setSendText={setSendText}
                                updateAuth={updateAuth}
                            >

                            </LoginMobile>
                        </>
                    )
                }




            </div>
        </>
    )
}



const Timer = (props: any) => {
    // initialize timeLeft with the seconds prop
    const [timeLeft, setTimeLeft] = React.useState(props.timeCountDown);

    React.useEffect(() => {
        // exit early when we reach 0
        if (!timeLeft) {
            props.setDisabled(false)
            return
        }

        // save intervalId to clear the interval when the
        // component re-renders
        const intervalId = setInterval(() => {
            setTimeLeft(timeLeft - 1);
        }, 1000);

        // clear interval on re-render to avoid memory leaks
        return () => clearInterval(intervalId);
        // add timeLeft as a dependency to re-rerun the effect
        // when we update it
    }, [timeLeft]);

    return (

        <span> {timeLeft} </span>

    );
};
